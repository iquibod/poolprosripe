import { Component, OnInit, TemplateRef, Pipe, PipeTransform } from '@angular/core';
import { ParamMap } from '@angular/router';

import 'rxjs/add/observable/of';


@Pipe({
    name: 'myfilter',
    pure: false
})
export class FilterDealers implements PipeTransform {
    transform(items: any[], filter: Object[]): any {
        
        if (typeof items === 'object') {
            if (filter.length === 0) {
                return items;
            } else {
               
               return items.filter(
                   item => filter.some(
                       f => item.data.certifications.includes( f )
                   )
               );
            }
        } else{
            return items;
        }
        
    }
}