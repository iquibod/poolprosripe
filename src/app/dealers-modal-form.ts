export class DealersModalForm {

	constructor(
	    public id: number,
	    public firstname: string,
	    public lastname: string,
	    public phonenumber: string,
	    public email: string,
	    public comments: string,
	    public ownedApool?: string
	) {  }

}
