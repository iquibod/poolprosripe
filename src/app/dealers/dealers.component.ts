import { Component, OnInit, TemplateRef, Pipe, PipeTransform } from '@angular/core';

import { DealersService } from '../dealers.service';
import { Dealers } from '../dealers';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { DealersModalComponent } from '../dealers-modal/dealers-modal.component';


@Component({
  selector: 'app-dealers',
  templateUrl: './dealers.component.html',
  styleUrls: ['./dealers.component.scss']
})

export class DealersComponent implements OnInit {

	filterargs = [];
	dealers = [];
	selectedDealers = Dealers;
	bsModalRef: BsModalRef;

	constructor(
		private dealersService: DealersService,
		private modalService: BsModalService,
		private data: DealersService,
	) { }

	ngOnInit() {
		this.getDealers();
	}

	optionSelectedReceive( $event ){
		this.filterargs = $event;
	}

	getDealers(): void{
		this.dealersService.getDealers()
			.subscribe(response => this.dealers = response['dealers']);
	}

	openModalWithComponent(data) {

		const initialState = {
		  list: [
		    data
		  ],
		  title: 'Modal Pros'
		};

		this.bsModalRef = this.modalService.show(DealersModalComponent, {initialState});
		this.bsModalRef.content.closeBtnName = 'Close';
	}
	
}
