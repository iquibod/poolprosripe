import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { HttpModule }      from '@angular/http';
import { CommonModule } from '@angular/common';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AccordionModule } from 'ngx-bootstrap/accordion';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DealersComponent } from './dealers/dealers.component';

import { DealersService } from './dealers.service';
import { FilterComponent } from './filter/filter.component';
import { AccordionComponent } from './accordion/accordion.component';
import { AppRoutingModule } from './/app-routing.module';
import { DealersDetailComponent } from './dealers-detail/dealers-detail.component';
import { SuppliesComponent } from './supplies/supplies.component';
import { ResourcesComponent } from './resources/resources.component';
import { ServicesComponent } from './services/services.component';

import { FilterDealers } from './filterDealers.pipe';

import { FilterDealersLetter } from './filterDealersLetter.pipe';
import { DealersModalComponent } from './dealers-modal/dealers-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    DealersComponent,
    FilterComponent,
    AccordionComponent,
    DealersDetailComponent,
    SuppliesComponent,
    ResourcesComponent,
    ServicesComponent,
    FilterDealers,
    FilterDealersLetter,
    DealersModalComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    NgbModule.forRoot(),
    AccordionModule.forRoot(),
    AppRoutingModule,

  ],
  exports: [BsDropdownModule, TooltipModule, ModalModule, AccordionModule],
  providers: [ DealersService ],
  bootstrap: [AppComponent],
  entryComponents: [DealersModalComponent]
})
export class AppModule { }
