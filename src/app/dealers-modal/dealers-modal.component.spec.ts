import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealersModalComponent } from './dealers-modal.component';

describe('DealersModalComponent', () => {
  let component: DealersModalComponent;
  let fixture: ComponentFixture<DealersModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealersModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealersModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
