import { Component, OnInit, TemplateRef, Pipe, PipeTransform, Input, EventEmitter, Output } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { DealersModalForm }    from '../dealers-modal-form';

@Component({
  selector: 'app-dealers-modal',
  templateUrl: './dealers-modal.component.html',
  styleUrls: ['./dealers-modal.component.scss']
})
export class DealersModalComponent implements OnInit {

	title: string;
	closeBtnName: string;
	list: any[] = [];

	message: string;

	constructor(
		public bsModalRef: BsModalRef,
	) { }

	ownedApool = [ 'Yes', 'No' ];

	// model = new Hero(18, 'Dr IQ', this.powers[0], 'Chuck Overstreet');

	model = new DealersModalForm(18, 'Ivan', 'Quibod', '12345678910', 'iquibod@gmail.com', 'Walay forever!', this.ownedApool[0]);

	submitted = false;

	onSubmit() { this.submitted = true; }

	ngOnInit() {
	    console.log(this.list);
	}

}
