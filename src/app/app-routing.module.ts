import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Pages
import { AccordionComponent }      from './accordion/accordion.component';
import { DealersComponent }      from './dealers/dealers.component';
import { SuppliesComponent }      from './supplies/supplies.component';
import { ResourcesComponent }      from './resources/resources.component';
import { ServicesComponent }      from './services/services.component';

const routes: Routes = [
	{ path: '', redirectTo: '/', pathMatch: 'full' },
	{ path: 'accordion', component: AccordionComponent  },
	{ path: 'supplies', component: SuppliesComponent  },
	{ path: 'resources', component: ResourcesComponent  },
	{ path: 'services', component: ServicesComponent  },
	{ path: '', component: DealersComponent },
];

@NgModule({
  imports: [
    // CommonModule
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ],
  declarations: []
})

export class AppRoutingModule { }