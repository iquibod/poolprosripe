import { Component, OnInit } from '@angular/core';

import { Hero }    from '../hero';

import { DealersModalForm }    from '../dealers-modal-form';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  powers = ['Really Smart', 'Super Flexible',
            'Super Hot', 'Weather Changer'];

  ownedApool = [ 'Yes', 'No' ];

  // model = new Hero(18, 'Dr IQ', this.powers[0], 'Chuck Overstreet');

  model = new DealersModalForm(18, 'Ivan', 'Quibod', '12345678910', 'iquibod@gmail.com', 'Walay forever!', this.ownedApool[0]);

  submitted = false;

  onSubmit() { this.submitted = true; }

  // TODO: Remove this when we're done
  get diagnostic() { return JSON.stringify(this.model); }

  // log(event: boolean) {
  //   console.log(`Accordion has been ${event ? 'opened' : 'closed'}`);
  // }

}
