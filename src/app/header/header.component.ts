import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

	ngOnInit() {
	}

	/* Open */

	public openNav() {
		// document.getElementById("myNav").style.height = "100%";
		console.log('open');
		$("#myNav").css({ 'height': "100%" });
		// $("#myNav").show();
	}

	/* Close */
	public closeNav() {
		// document.getElementById("myNav").style.height = "0%";
		console.log('close');

		$("#myNav").css({ 'height': "0%"});
		// $("#myNav").hide();
	}

}
