import { Component, OnInit, TemplateRef, Pipe, PipeTransform } from '@angular/core';
import { ParamMap } from '@angular/router';

import 'rxjs/add/observable/of';

@Pipe({
    name: 'orderByAlphabetically',
    pure: false
})

export class FilterDealersLetter implements PipeTransform {
    transform(array: Array<string>, args: string): Array<string> {

        
        array.sort( (a: any, b: any) => {

            if ( a.data[args] < b.data[args] ){
                return -1;
            }else if( a.data[args] > b.data[args] ){
                return 1;
            }else{
                return 0;    
            }
            
        });

        // console.log(array);
        return array;
    }
}