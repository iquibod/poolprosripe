import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  
  // @Output() outputToParent = new EventEmitter();
    @Input() dealers: any;
	@Output() selectedOutputParent: EventEmitter<any> = new EventEmitter<any>();
	

	constructor() { }

	options = [
		{name:'Service', value:'Service Pro', checked:false},
		{name:'Installation', value:'Installation Pro', checked:false},
		{name:'Residential', value:'Residential Pro', checked:false},
		{name:'Commercial', value:'Commercial Pro', checked:false}
	];

	filterargs = [];

	ngOnInit() {
	}

	get selectedOptions() { // right now: ['1','3']
	    return this.options
	              .filter(opt => opt.checked)
	              .map(opt => opt.value);
	}

	public displaySelected(): void{
		this.filterargs = this.selectedOptions;
		this.selectedOutputParent.emit(this.filterargs);
	}

}
